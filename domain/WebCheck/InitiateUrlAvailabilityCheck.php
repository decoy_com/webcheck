<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 13:42
 */

namespace Domain\WebCheck;


class InitiateUrlAvailabilityCheck implements CommandHandler
{
    const EMPTY_STR = 'URL list was empty!';
    const DONE_STR = 'web URL(s) availability are checked!';
    /**
     * @var UrlRepository
     */
    private $urlRepository;
    /**
     * @var Notification
     */
    private $notification;
    /**
     * @var AvailabilityChecker
     */
    private $availabilityChecker;


    /**
     * InitiateUrlAvailabilityCheck constructor.
     * @param UrlRepository $urlRepository
     * @param Notification $notification
     * @param AvailabilityChecker $availabilityChecker
     */
    public function __construct(UrlRepository $urlRepository, Notification $notification, AvailabilityChecker $availabilityChecker)
    {
        $this->urlRepository = $urlRepository;
        $this->notification = $notification;
        $this->availabilityChecker = $availabilityChecker;
    }

    /**
     * @param $input
     * @return bool
     */
    public function handle($input)
    {
        $list = $this->urlRepository->list();
        if (empty($list)) {
            $this->notification->notify(NotificationType::EMPTY_LIST, self::EMPTY_STR);
            return true;
        }
        foreach ($list as $item) {
            if(!$this->availabilityChecker->check($item)) {
                $this->notification->notify(NotificationType::UNAVAILABLE, $item->getUrl());
            }
        }
        $count = count($list);
        $this->notification->notify(NotificationType::DONE, "$count " . self::DONE_STR);
        return true;
    }
}