<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 12:47
 */

namespace Domain\WebCheck;


class AddUrl implements CommandHandler
{
    /**
     * @var UrlRepository
     */
    private $urlRepository;

    /**
     * AddUrl constructor.
     * @param UrlRepository $urlRepository
     */
    public function __construct(UrlRepository $urlRepository)
    {
        $this->urlRepository = $urlRepository;
    }

    /**
     * @param Url $input
     * @return bool
     * @throws InvalidDomainInput
     */
    public function handle($input)
    {
        if(!($input instanceof Url)) {
            throw new InvalidDomainInput('Url is invalid');
        }
        if ($this->urlRepository->contains($input)) {
            return false;
        }
        return $this->urlRepository->save($input);
    }
}