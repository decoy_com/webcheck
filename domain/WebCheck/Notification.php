<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 14:23
 */

namespace Domain\WebCheck;


interface Notification
{
    /**
     * @param string $type
     * @param string $message
     */
    public function notify($type, $message);
}