<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 13:15
 */

namespace Domain\WebCheck;


interface UrlRepository
{

    /**
     * @param Url $url
     * @return bool
     */
    public function save(Url $url);

    /**
     * @param Url $url
     * @return bool
     */
    public function contains(Url $url);

    /**
     * @return Url[]
     */
    public function list();
}