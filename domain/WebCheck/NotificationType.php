<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 14:28
 */

namespace Domain\WebCheck;


class NotificationType
{
    const DONE = 'done';
    const EMPTY_LIST = 'empty';
    const UNAVAILABLE = 'unavailable';
}