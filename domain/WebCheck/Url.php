<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 13:07
 */

namespace Domain\WebCheck;


class Url
{
    /**
     * @var string
     */
    private $url;

    /**
     * Url constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}