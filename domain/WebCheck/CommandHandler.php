<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 13:10
 */

namespace Domain\WebCheck;


interface CommandHandler
{
    /**
     * @param $input
     * @return bool
     */
    public function handle($input);
}