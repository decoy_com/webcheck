<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 14:52
 */

namespace Domain\WebCheck;


interface AvailabilityChecker
{

    /**
     * @param Url $item
     * @return bool
     */
    public function check(Url $item): bool;
}