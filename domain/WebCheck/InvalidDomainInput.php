<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 14:17
 */

namespace Domain\WebCheck;


class InvalidDomainInput extends \Exception
{

    /**
     * InvalidDomainInput constructor.
     * @param string $string
     */
    public function __construct($string)
    {
        parent::__construct($string);
    }
}