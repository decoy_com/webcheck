<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 19:59
 */

namespace Application\WebCheck;

use Domain\WebCheck\Url;
use PHPUnit\Framework\TestCase;

class SimpleAvailabilityCheckTest extends TestCase
{

    public function test_file_unavailable()
    {
        $target = new SimpleAvailabilityCheck;

        $file = 'not.file.content';

        self::assertFalse($target->check(new Url($file)));
    }

    public function test_file_available()
    {
        $target = new SimpleAvailabilityCheck;

        $file = 'file.content';

        file_put_contents($file,'data');

        self::assertTrue($target->check(new Url($file)));

        unlink($file);
    }
}
