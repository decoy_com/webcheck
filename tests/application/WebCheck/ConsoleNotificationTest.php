<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 15:23
 */

namespace Application\WebCheck;

use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ConsoleNotificationTest extends TestCase
{
    public function test_notify()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $target = new ConsoleNotification($logger);

        $type = 'test';
        $message = 'message';
        $logger
            ->expects($this->once())
            ->method('info')
            ->with("[$type] $message");

        $target->notify($type, $message);
    }
}
