<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 20:02
 */

namespace Application\WebCheck;

use Domain\WebCheck\Url;
use TestCase;

//use PHPUnit\Framework\TestCase;

class EloquentUrlRepositoryTest extends TestCase
{

    public function test_save_url()
    {
        (new UrlRecordTable)->newQuery()->truncate();

        $target = new EloquentUrlRepository();
        $url = new Url('utl://something');

        self::assertTrue($target->save($url));

        self::assertEquals([$url], $target->list());
    }

    public function test_contains()
    {
        (new UrlRecordTable)->newQuery()->truncate();

        $target = new EloquentUrlRepository();
        $url = new Url('utl://something-new');
        $urlOld = new Url('utl://something-old');

        self::assertTrue($target->save($url));
        self::assertTrue($target->contains($url));
        self::assertFalse($target->contains($urlOld));
    }


}
