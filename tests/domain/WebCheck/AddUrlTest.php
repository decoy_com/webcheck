<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 12:53
 */

namespace Domain\WebCheck;


use PHPUnit\Framework\TestCase;

class AddUrlTest extends TestCase
{
    /**
     * @test
     * @expectedException \Domain\WebCheck\InvalidDomainInput
     */
    public function test_handle_with_other_than_url()
    {
        $urlRepo = $this->createMock(UrlRepository::class);
        $target = New AddUrl($urlRepo);
        $input = 'a string';

        $result = $target->handle($input);

        self::assertTrue($result);
    }

    /**
     * @test
     */
    public function test_persists_url_the_first_time_with_success()
    {
        $urlRepo = $this->createMock(UrlRepository::class);
        $target = New AddUrl($urlRepo);
        $input = new Url('my url');

        $urlRepo
            ->expects($this->once())
            ->method('save')
            ->with($input)
            ->willReturn(true);

        $result = $target->handle($input);

        self::assertTrue($result);
    }

    /**
     * @test
     */
    public function test_wont_persists_same_url_twice()
    {
        $urlRepo = $this->createMock(UrlRepository::class);
        $target = New AddUrl($urlRepo);
        $input = new Url('my url');


        $urlRepo
            ->expects($this->exactly(2))
            ->method('contains')
            ->with($input)
            ->willReturnOnConsecutiveCalls(false, true);

        $urlRepo
            ->expects($this->once())
            ->method('save')
            ->with($input)
            ->willReturn(true);

        $result1 = $target->handle($input);
        $result2 = $target->handle($input);

        self::assertTrue($result1);
        self::assertFalse($result2);
    }
}


