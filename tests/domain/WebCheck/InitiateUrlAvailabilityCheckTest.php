<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 13:45
 */

namespace Domain\WebCheck;

use PHPUnit\Framework\TestCase;

class InitiateUrlAvailabilityCheckTest extends TestCase
{

    public function test_empty_run()
    {
        $urlRepo = $this->createMock(UrlRepository::class);
        $notify = $this->createMock(Notification::class);
        $avail = $this->createMock(AvailabilityChecker::class);
        $target = new InitiateUrlAvailabilityCheck($urlRepo, $notify, $avail);

        $urlRepo
            ->expects($this->once())
            ->method('list')
            ->willReturn([]);
        $notify
            ->expects($this->once())
            ->method('notify')
            ->with(NotificationType::EMPTY_LIST, InitiateUrlAvailabilityCheck::EMPTY_STR);

        $target->handle(null);
    }

    public function test_one_available_element()
    {
        $urlRepo = $this->createMock(UrlRepository::class);
        $notify = $this->createMock(Notification::class);
        $avail = $this->createMock(AvailabilityChecker::class);
        $target = new InitiateUrlAvailabilityCheck($urlRepo, $notify, $avail);
        $url = new Url('a url');

        $urlRepo
            ->expects($this->once())
            ->method('list')
            ->willReturn([$url]);

        $avail
            ->expects($this->once())
            ->method('check')
            ->with($url)
            ->willReturn(true);

        $notify
            ->expects($this->once())
            ->method('notify')
            ->with(NotificationType::DONE, '1 ' . InitiateUrlAvailabilityCheck::DONE_STR);

        $target->handle(null);
    }

    public function test_one_unavailable_element()
    {
        $urlRepo = $this->createMock(UrlRepository::class);
        $notify = $this->createMock(Notification::class);
        $avail = $this->createMock(AvailabilityChecker::class);
        $target = new InitiateUrlAvailabilityCheck($urlRepo, $notify, $avail);
        $url = new Url('a url');

        $urlRepo
            ->expects($this->once())
            ->method('list')
            ->willReturn([$url]);

        $avail
            ->expects($this->once())
            ->method('check')
            ->with($url)
            ->willReturn(false);

        $notify
            ->expects($this->exactly(2))
            ->method('notify')
            ->withConsecutive([NotificationType::UNAVAILABLE, $url->getUrl()], [NotificationType::DONE, '1 ' . InitiateUrlAvailabilityCheck::DONE_STR]);

        $target->handle(null);
    }


}
