<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 19/03/2018
 * Time: 09:37
 */

namespace Framework;

use Domain\WebCheck\AddUrl;
use Domain\WebCheck\InitiateUrlAvailabilityCheck;
use Domain\WebCheck\Url;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class UrlControllerTest extends TestCase
{
    /** @var UrlController */
    private $urlController;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();
        $this->urlController = $controller = new UrlController();
    }

    public function test_add_url_with_empty_url()
    {
        $request = $this->createRequest('post', 'localhost/url');
        $addUrl = $this->createMock(AddUrl::class);

        $response = $this->urlController->postAddUrl($request, $addUrl);

        self::assertEquals(400, $response->getStatusCode());
    }

    public function test_add_url_with_empty_string_url()
    {
        $request = $this->createRequest('post', 'localhost/url',['url' => '']);
        $addUrl = $this->createMock(AddUrl::class);

        $response = $this->urlController->postAddUrl($request, $addUrl);

        self::assertEquals(400, $response->getStatusCode());
    }

    public function test_add_url_as_post_url()
    {
        $url = 'url://address';
        $request = $this->createRequest('post', 'localhost/url', ['url' => $url]);
        $addUrl = $this->createMock(AddUrl::class);
        $addUrl
            ->expects($this->once())
            ->method('handle')
            ->with(new Url($url))
            ->willReturn(true);

        $response = $this->urlController->postAddUrl($request, $addUrl);

        self::assertEquals(200, $response->getStatusCode());
    }

    public function test_url_check_activation()
    {
        $availabilityCheck = $this->createMock(InitiateUrlAvailabilityCheck::class);

        $availabilityCheck
            ->expects($this->once())
            ->method('handle')
            ->with(null)
            ->willReturn(true);

        $response = $this->urlController->getUrls($availabilityCheck);

        self::assertEquals(200, $response->getStatusCode());
    }


    private function createRequest($method, $uri, $data = [], array $headers = [])
    {
        $symfonyRequest = SymfonyRequest::create(
            $uri, $method, $data,
            [], [], [], http_build_query($data)
        );
        $request = Request::createFromBase($symfonyRequest);
        $request->headers = new HeaderBag($headers);
        return $request;
    }
}
