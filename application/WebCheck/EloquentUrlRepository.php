<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 15:31
 */

namespace Application\WebCheck;


use Domain\WebCheck\Url;
use Domain\WebCheck\UrlRepository;

class EloquentUrlRepository implements UrlRepository
{

    const DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @param Url $url
     * @return bool
     */
    public function save(Url $url)
    {
        $obj = (new UrlRecordTable);
        $obj->{UrlRecordTable::FIELD_URL} = $url->getUrl();
        $obj->{UrlRecordTable::CREATED_AT} = (new \DateTime)->format(self::DATETIME_FORMAT);
        return $obj->save();
    }

    /**
     * @param Url $url
     * @return bool
     */
    public function contains(Url $url)
    {
        $obj = (new UrlRecordTable)->newQuery()->where(UrlRecordTable::FIELD_URL, $url->getUrl())->first();
        return !empty($obj);
    }

    /**
     * @return Url[]
     */
    public function list()
    {
        $array = (new UrlRecordTable)->newQuery()->pluck(UrlRecordTable::FIELD_URL, 'id')->toArray();
        $result = [];
        foreach ($array as $item) {
            $result[] = new Url($item);
        }
        return $result;
    }
}