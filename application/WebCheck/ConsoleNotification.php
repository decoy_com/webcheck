<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 15:20
 */

namespace Application\WebCheck;

use Domain\WebCheck\Notification;
use Psr\Log\LoggerInterface;

class ConsoleNotification implements Notification
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ConsoleNotification constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $type
     * @param string $message
     */
    public function notify($type, $message)
    {
        $dt = (new \DateTime)->format('c');
        $this->logger->info("[$type] $message");
        $this->latestTime = $dt;
    }
}