<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 15:44
 */

namespace Application\WebCheck;


use Illuminate\Database\Eloquent\Model;

class UrlRecordTable extends Model
{
    const CREATED_AT = 'created_at';
    const FIELD_ID = 'id';
    const FIELD_URL = 'url';

    protected $table = 'url_record';

    /**
     * Set the value of the "updated at" attribute.
     *
     * @param  mixed $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        return $this;
    }

    /**
     * Disable update_at when update
     * @return null
     */
    public function getUpdatedAtColumn()
    {
        return null;
    }
}