<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 15:59
 */

namespace Application\WebCheck;


use Domain\WebCheck\AvailabilityChecker;
use Domain\WebCheck\Url;

class SimpleAvailabilityCheck implements AvailabilityChecker
{
    /**
     * @param Url $item
     * @return bool
     */
    public function check(Url $item): bool
    {
        try {
            $result = file_get_contents($item->getUrl());
        } catch (\Exception $e) {
            return false;
        }
        if($result === false) {
            return false;
        }
        return true;
    }
}