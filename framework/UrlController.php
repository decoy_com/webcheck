<?php

namespace Framework;

use App\Http\Controllers\Controller;
use Domain\WebCheck\AddUrl;
use Domain\WebCheck\InitiateUrlAvailabilityCheck;
use Domain\WebCheck\Url;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 18/03/2018
 * Time: 20:36
 */
class UrlController extends Controller
{
    /**
     * @param Request $request
     * @param AddUrl $addUrl
     * @return Response
     */
    public function postAddUrl(Request $request, AddUrl $addUrl)
    {
        $url = $request->post('url');
        if (empty($url)) {
            return $this->createResponse(400);
        }
        if ($addUrl->handle(new Url($url))) {
            return $this->createResponse(200);
        }
        return $this->createResponse(400);
    }

    /**
     * @param InitiateUrlAvailabilityCheck $availabilityCheck
     * @return Response
     */
    public function getUrls(InitiateUrlAvailabilityCheck $availabilityCheck)
    {
        $availabilityCheck->handle(null);
        return $this->createResponse(200);
    }

    /**
     * @param $status
     * @param $statusMessage
     * @param array|string $content
     * @param array $headers
     * @return Response
     */
    private function createResponse($status, $statusMessage = null, $content = '', $headers = []): Response
    {
        $response = new Response;
        $response->setStatusCode($status, $statusMessage);
        $response->setContent($content);
        $response->withHeaders($headers);
        return $response;
    }
}